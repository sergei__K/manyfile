

import java.io.*;

/**
 * Класс читает шестизначные числа из файла int6data.dat , отбирает из них счастливые и записывает их в файл Txt6data.txt.
 *
 * @author Коленционок С.А.
 */
public class Txt6data {
    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("E:\\work\\Manyfilse\\src\\Txt6data.dat"));
        DataInputStream dataInputStream = new DataInputStream(new FileInputStream("E:\\work\\Manyfilse\\src\\int6data.dat"));
        int number;
        while (dataInputStream.available() > 0) {
            if (luck(number = dataInputStream.readInt())) {
                bufferedWriter.write(number + "\n");
            }
        }
        bufferedWriter.close();
        dataInputStream.close();
    }

    /**
     * Метод для отбора счастливых билетов
     */
    public static boolean luck(int number) {
        return (number / 100000 + number % 100000 / 10000 + number % 10000 / 1000) == (number % 1000 / 100 + number % 100 / 10 + number % 10);
    }

}
