


import java.io.*;

/**
 * Класс для чтения целых чисел из файла intdata.dat , отбирает шестизначные числа и записывает их в файл int6data.dat .
 *
 * @author Коленционок С.А.
 */
public class Int6data {
    public static void main(String[] args) throws IOException {
        DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream("E:\\work\\Manyfilse\\src\\int6data.dat"));
        DataInputStream dataInputStream = new DataInputStream(new FileInputStream("E:\\work\\Manyfilse\\src\\intdata.dat"));
        int number;
        while ((dataInputStream.available()) > 0) {
            number = dataInputStream.readInt();
            if (number > 1000 && number < 1000000) {
                dataOutputStream.writeInt(number);
            }
        }
        dataOutputStream.close();
        dataInputStream.close();
    }
}
