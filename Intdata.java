

import java.io.*;

/**
 * Класс предназначен для чтения чисел из файла txt.txt и записи в файл intdata.dat.
 *
 * @author Коленционок С.А.
 */
public class Intdata {
    public static void main(String[] args) throws IOException {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("E:\\work\\Manyfilse\\src\\txt.txt"));
             DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream(new File("E:\\work\\Manyfilse\\src\\intdata.dat")))) {
            String string;
            while ((string = bufferedReader.readLine()) != null) {
                try {
                    dataOutputStream.writeInt(Integer.valueOf(string));
                } catch (NumberFormatException e) {
                }
            }
        }
    }
}
